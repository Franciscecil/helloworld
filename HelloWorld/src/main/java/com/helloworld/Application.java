/**
 * 
 */
package com.helloworld;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);

	}
		
	 /*@Bean
	    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
	        return args -> {//Lambda Expression 
	            System.out.println("**********************Spring Beans****************");
	            String[] beanNames = ctx.getBeanDefinitionNames();
	            Arrays.sort(beanNames);
	            for (String beanName : beanNames) {
	                System.out.println(beanName);
	            }
	            System.out.println("**********************Spring Beans****************");
	        };
	        
	    }*/
}