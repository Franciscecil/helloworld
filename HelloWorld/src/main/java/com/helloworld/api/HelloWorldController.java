package com.helloworld.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

	@GetMapping(path="/gethello")
	public ResponseEntity<String>  getResources(@RequestParam(value="id",required=true)String param){
		System.out.println("inside getHello");
		return new ResponseEntity<String>(param,HttpStatus.OK);	
	}

}